var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var cors = require('cors');
app.use(cors());
var DButilsAzure = require('./DButils');
var users = require('./Modules/Users');
var poi = require('./Modules/POI');
var review = require('./Modules/Review');
var registerUsers = require('./Modules/RegisterUsers');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.use('/Users', users);
app.use('/POI', poi);
app.use('/RegisterUsers', registerUsers);
app.use('/Review', review);


var port = 3000;
app.listen(port, function () {
    console.log('listening on port ' + port);
});

