var express = require('express');
var app = express.Router();
var DButilsAzure = require('../DButils');
var jwt = require('jsonwebtoken');

app.get('/exploringPointsOfinterest', function (req, res) {

    DButilsAzure.execQuery("SELECT p.id,p.picture, p.name,p.categoryID ,p.shortDesc,p.rank FROM dbo.Points as p WHERE rank > 3")
        .then(function (result) {
            if (result.length > 3) {
                var keyArray = Object.keys(result);

                var index = Math.floor(Math.random() * keyArray.length);
                var keysIndex = keyArray[index];
                keyArray.splice(index, 1);
                var first = result[keysIndex];

                index = Math.floor(Math.random() * keyArray.length);
                keysIndex = keyArray[index];
                keyArray.splice(index, 1);
                var second = result[keysIndex];

                index = Math.floor(Math.random() * keyArray.length);
                keysIndex = keyArray[index];
                keyArray.splice(index, 1);
                var third = result[keysIndex];

                res.send([first, second, third]);
            }
            else {

                if (result.length > 2)
                    res.send([result[0], result[1], result[2]]);
                else if (result.length > 1)
                    res.send([result[0], result[1]]);
                else if (result.length > 0)
                    res.send([result[0]]);
            }
        })
        .catch(function (err) {
            res.send('cant find exploringPointsOfinterest');
        });
});

app.get('/allPointsOfInterest', function (req, res) {
    DButilsAzure.execQuery("select p.id , p.picture, p.name, p.categoryID,p.shortDesc,p.rank, cg.name as catName from points as p inner join Categories as cg ON p.categoryID=cg.id")
        .then(function (result) {
            res.send(result);
        })
        .catch(function (err) {
            res.json({
                success: false,
                messege: "cant return all Points Of Interest"
            });
        })
})

app.get('/fullPointData/:pointId', function (req, res) {
    if (!req.params.pointId) {
        res.json({
            success: false,
            messege: "pointId is missing"
        });
    }
    else {
        DButilsAzure.execQuery("select p.id,p.picture, p.name, p.categoryID, p.viewsNum,p.shortDesc, p.fullDesc,p.rank,p.numberOfVoters, ploc.location as location, first.firstReview, first.firstDate,second.secondReview, second.secondDate from points as p inner join POILocation as ploc ON p.id=ploc.poiID left outer join(select top 1 pr.date firstDate, pr.review as firstReview, pr.id, pr.pointId from PointReviews as pr where pr.pointId = "+req.params.pointId+" order by date desc) as first on p.id = first.pointId left outer  join(select top 1 pr.date secondDate, pr.review as secondReview, pr.id, pr.pointId from PointReviews as pr where pr.pointId = "+req.params.pointId+"  and  pr.id not in (select a.ri from(select top 1 pr.id as ri from PointReviews as pr where pr.pointId = "+ req.params.pointId+" order by date desc) as a) order by date desc) as second on  first.pointId = second.pointId where p.id = " + req.params.pointId)
            .then(function (result) {
                if (result.length > 0) {

                    DButilsAzure.execQuery("UPDATE points SET viewsNum = " + (result[0].viewsNum + 1) + " WHERE id = " + result[0].id)
                        .then(function (result) {
                        })
                        .catch(function (err) {
                            res.json({
                                success: false,
                                messege: "cant update point number ofViews"
                            });
                        })

                    res.send(result);
                }
                else {
                    res.json({
                        success: false,
                        messege: "PointId dose not exist"
                    });
                }
            })
            .catch(function (err) {
                res.json({
                    success: false,
                    messege: "PointId is not correct"
                });
            })
    }
})

app.get('/allPointsOfInterest/:categotyId', function (req, res) {
    if (!req.params.categotyId) {
        res.json({
            success: false,
            messege: "categotyId is missing"
        });
    }
    else {
        DButilsAzure.execQuery("select p.id , p.picture, p.name, p.categoryID,p.shortDesc,p.rank from points as p where categoryId =" + req.params.categotyId)
            .then(function (result) {
                if (result.length > 0) {
                    res.send(result);
                }
                else {
                    res.json({
                        success: false,
                        messege: "categotyId does not exist"
                    });
                }
            })
            .catch(function (err) {
                res.json({
                    success: false,
                    messege: "categotyId is not correct"
                });
            })
    }
})

app.get('/allCategories', function name(req, res) {
    DButilsAzure.execQuery("select * from Categories")
        .then(function (result) {
            res.send(result);
        })
        .catch(function (err) {
            res.json({
                success: false,
                messege: "cant return all Categories "
            });
        })
})















module.exports = app;