
var express = require('express');
var app = express.Router();
var DButilsAzure = require('../DButils');
var jwt = require('jsonwebtoken');
var superSecret = 'delite';

//make sure the user really loged in
app.use(function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        // verifies secret
        jwt.verify(token, superSecret, function (err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                var decoded = jwt.decode(token, { complete: true });
                req.userName = decoded.payload.userName;
                next();
            }
        });

    } else {
        // if there is no token return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    };
});

app.get('/popularPointsOfInterestByUserCategories', function (req, res) {

    DButilsAzure.execQuery("select p.id , p.picture, p.name, p.categoryID,p.shortDesc,p.rank from points as p where id in((select top 1 p.id from points as p inner join (select top 1 p.categoryID , max(rank)  as rank from points as p  where categoryID in (select categoryId from UserCategories where UserName= '" + req.userName + "')  and categoryID not in (select a.ca from (select top 1 categoryID as ca, max(rank) as rank from points where categoryID in (select categoryId from UserCategories where UserName = '" + req.userName + "') group By categoryID order by rank desc) as a) group By p.categoryID order by rank desc) as second on p.categoryID = second.categoryID and p.rank = second.rank ), (select top 1 p.id from points as p inner join  (select top 1 p.categoryID , max(rank)  as rank from points as p where categoryID in (select categoryId from UserCategories where UserName = '" + req.userName + "') group By p.categoryID order by rank desc) as first on p.categoryID = first.categoryID and p.rank = first.rank))")
        .then(function (result) {
            res.send(result);
        })
        .catch(function (err) {
            res.json({
                success: false,
                messege: "cant return popular Points Of Interest By User Categories"
            });
        })
});

app.get('/lastPointsOfInterestSaved', function (req, res) {

    DButilsAzure.execQuery("select top 2  p.id , p.picture, p.name, p.categoryID,p.shortDesc,p.rank from points as p where id in (select top 2 pointId from UserFavoritePoints where UserName = '" + req.userName + "' order by place desc )")
        .then(function (result) {
            res.send(result);
        })
        .catch(function (err) {
            res.json({
                success: false,
                messege: "cant return Last Points Of Interest Saved by the user"
            });
        })
});

app.get('/favoritesPointsOfInterest', function (req, res) {

    DButilsAzure.execQuery("select p.id, p.picture, p.name, p.categoryID, p.shortDesc, p.rank, ufp.place, cg.name as cName from points as p inner join UserFavoritePoints as ufp ON p.id = ufp.pointId inner join Categories as cg ON p.categoryID = cg.id where ufp.UserName ='" + req.userName + "' and p.id in (select pointId from UserFavoritePoints where UserName = '" + req.userName + "') ORDER BY ufp.place")
        .then(function (result) {
            res.send(result);
        })
        .catch(function (err) {
            res.json({
                success: false,
                messege: "cant return the user's favorites Points Of Interest"
            });
        })
});

app.put('/updateFavoritePointsOfInterestInSpecificOrder', function (req, res) {

    if (!req.body.array) {
        res.json({
            success: false,
            messege: "array is missing"
        });
    }
    else {
        try {
            var parsedPoints = req.body.array

            DButilsAzure.execQuery("DELETE FROM UserFavoritePoints WHERE UserName='" + req.userName + "' ")
                .then(function (result) {
                    for (let index = 0; index < parsedPoints.length; index++) {
                        if (parsedPoints[index].pointId && parsedPoints[index].place) {
                            DButilsAzure.execQuery("insert into UserFavoritePoints values (" + parsedPoints[index].pointId + "," + parsedPoints[index].place + ",'" + req.userName + "') ")
                                .then(function (result) {
                                })
                                .catch(function (err) {
                                })
                        }
                    }
                    res.json({
                        success: true,
                        messege: "all the correct points were saved"
                    });
                })
                .catch(function (err) {
                    res.json({
                        success: false,
                        messege: "problem with saving a point of interest"
                    });
                })

        } catch (error) {
            res.json({
                success: false,
                messege: "problem with array format"
            });
        }

    }
});

app.put('/addFavoritePointOfInterestToEndOfList', function (req, res) {

    if (!req.body.pointId) {
        res.json({
            success: false,
            messege: "pointId is missing"
        });
    }
    else {
        DButilsAzure.execQuery("insert into UserFavoritePoints values (" + req.body.pointId + ",(1 +  Isnull((select top 1 place from UserFavoritePoints where UserName = '" + req.userName + "' order by place desc),0)),'" + req.userName + "')")
            .then(function (result) {
                res.json({
                    success: true,
                    messege: "saved successfuly"
                });
            })
            .catch(function (err) {
                res.json({
                    success: false,
                    messege: "cant add this Favorite Point Of Interest To End Of List"
                });
            })

    }
});

app.post('/removeFavoritePointOfInterest', function (req, res) {

    if (!req.body.pointId) {
        res.json({
            success: false,
            messege: "pointId is missing"
        });
    }
    else {
        DButilsAzure.execQuery("delete from UserFavoritePoints where pointId = "+ req.body.pointId +" and userName = '"+ req.userName +"'")
            .then(function (result) {
                res.json({
                    success: true,
                    messege: "removed successfuly"
                });
            })
            .catch(function (err) {
                res.json({
                    success: false,
                    messege: "cant remove this Favorite Point Of Interest from the user"
                });
            })

    }
});

module.exports = app;