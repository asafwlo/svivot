var express = require('express');
var app = express.Router();
var DButilsAzure = require('../DButils');
var jwt = require('jsonwebtoken');

var fs = require('fs');
var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var userName;
var superSecret = 'delite';

// Register new user
app.post('/registerNewUser', function(req, res) {
    if (!req.body.firstName || !req.body.lastName || !req.body.City || !req.body.Country || !req.body.Email) {
        res.json({
            success: false,
            messege: "parameters are missing"
        });
    }
    else {
        var password = generatePassword();
        DButilsAzure.execQuery("SELECT dbo.Users.userName FROM dbo.Users WHERE dbo.Users.userName like '" + req.body.firstName.substring(0, 2) + req.body.lastName.substring(0, 2) + "%'")
            .then(function(result) {
                var foundName = false;
                while (!foundName) {
                    userName = req.body.firstName.substring(0, 2) + req.body.lastName.substring(0, 2) + Math.floor(Math.random() * 100);
                    foundName = true;
                   // console.log('the results.username:' + result[0].userName)
                    for (var i = 0; i<result.length;i++) {
                        //console.log('the value:' + result[i].userName)
                        if (result[i].userName === userName)
                            foundName = false;
                    }
                }

                var query = "INSERT INTO dbo.UserCategories VALUES ";
                var temp = query;
                var categories = JSON.parse(req.body.categories);
                for (var key in categories) {
                    query = temp;
                    if (categories.hasOwnProperty(key)) {
                        query = query + "('" + categories[key] + "','" + userName + "')";
                    }
                    temp = query + ",";
                }

                DButilsAzure.execQuery("INSERT INTO dbo.Users VALUES ('" + userName + "','" + req.body.firstName + "','" + req.body.lastName + "','" + req.body.City + "','" + req.body.Country + "','" + req.body.Email + "','" + password + "')")
                    .then(DButilsAzure.execQuery("INSERT INTO dbo.UserAnswers VALUES ('" + req.body.q1 + "','" + req.body.ans1 + "','" + userName + "'),('" + req.body.q2 + "','" + req.body.ans2 + "','" + userName + "')",1500)
                        .then(DButilsAzure.execQuery(query,1500)
                            .then(function(result) {
                                console.log('resolve: "userName":' +userName+', "password":'+ password );
                                res.send([{ "userName": userName, "password": password }]);
                            })
                            .catch(function(err) {
                                console.log(err + 'catch1');
                                res.json({
                                    success: false,
                                    messege: "Couldnt find user"
                                });
                            })
                        )
                        .catch(function(err) {
                            console.log(err + 'catch2');
                            res.json({
                                success: false,
                                messege: "Couldnt find user"
                            });
                        })
                    ).catch(function(err) {
                        console.log(err + 'catch3');
                        res.json({
                            success: false,
                            messege: "Couldnt find user"
                        });
                    });
            })
            .catch(function(err) {
                console.log(err + 'catch4');
                res.json({
                    success: false,
                    messege: "Couldnt find user"
                });
            });

    }
});

// Login user
app.post('/login', function(req, res) {
    if (!req.body.userName || !req.body.password) {
        res.json({
            success: false,
            messege: "userName and password is required"
        });
    }
    else {
        var userName = req.body.userName;
        var password = req.body.password;

        DButilsAzure.execQuery("SELECT dbo.Users.userName FROM dbo.Users WHERE (dbo.Users.userName='" + userName + "' AND dbo.Users.password='" + password + "')")
            .then(function(result) {
                if (result.length > 0) {
                    console.log('success');
                    var payload = {
                        userName: result[0].userName,
                    }

                    var token = jwt.sign(payload, superSecret, {
                        expiresIn: '1d'
                    });

                    res.json({
                        success: true,
                        token: token
                    });
                }
                else
                    res.json({
                        success: false,
                        token: null
                    });
            })
            .catch(function(err) {
                res.json({
                    success: false,
                    messege: "Couldnt find user"
                });
            });
    }

});

app.post('/restorePassword', function(req, res) {
    var userName = req.body.userName;
    var q1 = req.body.q1;
    var q2 = req.body.q2;
    var ans1 = req.body.ans1;
    var ans2 = req.body.ans2;
    if (!userName || !q1 || !q2 || !ans1 || !ans2) {
        res.json({
            success: false,
            message: "parameters are missing"
        });
    }
    else {
        DButilsAzure.execQuery("SELECT dbo.UserAnswers.questionId, dbo.UserAnswers.ans, dbo.Users.password FROM dbo.UserAnswers Inner JOIN dbo.Users ON dbo.Users.userName=dbo.UserAnswers.userName WHERE (dbo.Users.userName='" + userName + "' AND questionId IN ('" + q1 + "','" + q2 + "'))")
            .then(function(result) {
                if (result.length === 2 && ((ans1 == result[0].ans && q1 == result[0].questionId && ans2 == result[1].ans && q2 == result[1].questionId) || (ans1 == result[1].ans && q1 == result[1].questionId && ans2 == result[0].ans && q2 == result[0].questionId))) {
                    res.json({
                        success: true,
                        password: result[0].password
                    });

                }
                else {
                    res.json({
                        success: false,
                        password: null
                    });
                }
            })
            .catch(function(err) {
                res.json({
                    success: false,
                    password: null
                });
            });
    }
});

// generate password
function generatePassword() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 7; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

app.post('/QustionsByUserName', function(req, res) {
    if (!req.body.userName) {
        res.json({
            success: false,
            messege: "userName is required"
        });
    }
    else {
        DButilsAzure.execQuery(" select top 2 ua.questionId,q.text from userAnswers as ua join questions as q on q.id = ua.questionId where userName = '" + req.body.userName + "'")
            .then(function(result) {
                res.send(result);
            })
            .catch(function(err) {
                res.json({
                    success: false,
                    messege: "cant return all Categories "
                });
            })
    }
})

app.get('/CountriesOptionalls', function(req, res) {
    fs.readFile('countries.xml', function(err, data) {
        parser.parseString(data, function(err, result) {
            console.log(result);
            res.send(result);
        });
    });
})

app.get('/allQuestionOptionalls', function(req, res) {
    DButilsAzure.execQuery("select * from Questions")
        .then(function(result) {
            res.send(result);
        })
        .catch(function(err) {
            res.json({
                success: false,
                messege: "cant return all Questions "
            });
        })
})

module.exports = app;