
var express = require('express');
var app = express.Router();
var DButilsAzure = require('../DButils');
var jwt = require('jsonwebtoken');
var superSecret = 'delite';

//make sure the user really loged in
app.use(function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (token) {
        // verifies secret
        jwt.verify(token, superSecret, function (err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                var decoded = jwt.decode(token, { complete: true });
                req.userName = decoded.payload.userName;
                next();
            }
        });

    } else {
        // if there is no token return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    };
});

app.put('/addLiteralReviewToPointOfInterest', function (req, res) {
    if (!req.body.pointId || !req.body.review) {
        res.json({
            success: false,
            messege: "pointId and review are requierd"
        });
    }
    else {
        DButilsAzure.execQuery("insert into PointReviews values (" + req.body.pointId + ",GETDATE (),'" + req.body.review + " ')")
            .then(function (result) {
                res.json({
                    success: true,
                    messege: "the review saved successfully"
                });
            })
            .catch(function (err) {
                res.json({
                    success: false,
                    messege: "cant add Literal Review To Point Of Interest"
                });
            })
    }
})

app.put('/addRateToPointOfInterest', function (req, res) {
    if (!req.body.pointId || !req.body.rate) {
        res.json({
            success: false,
            messege: "pointId and rate are requierd"
        });
    }
    else {
        try {
        DButilsAzure.execQuery("select rank , numberOfVoters from points where id = " + req.body.pointId)
            .then(function (result) {
                if (result.length > 0) {
                    var numberOfVoters = parseInt(result[0].numberOfVoters);
                    var rankavg = parseInt(result[0].rank) * numberOfVoters;
                    rankavg = (rankavg + parseInt(req.body.rate)) / (numberOfVoters + 1);
                    DButilsAzure.execQuery("UPDATE points SET rank = " + rankavg + ", numberOfVoters =" + (numberOfVoters + 1) + " WHERE id =" + req.body.pointId)
                        .then(function (result) {
                            res.json({
                                success: true,
                                messege: "updated successfully"
                            });
                        })
                        .catch(function (err) {
                            res.json({
                                success: false,
                                messege: "cant update rate"
                            });
                        })
                }
                else {
                    res.json({
                        success: false,
                        messege: "pointId do not exist"
                    });
                }

            })
            .catch(function (err) {
                res.json({
                    success: false,
                    messege: "pointId is not correct"
                });
            })
        } catch (error) {
            res.json({
                success: false,
                messege: "rate must be an integer"
            });
        }

    }
})


module.exports = app;