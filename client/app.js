let app = angular.module('citiesApp', ['ngRoute', 'LocalStorageModule']);

app.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {

    $locationProvider.hashPrefix('');

    $routeProvider.when('/', {
        templateUrl: 'components/Main/main.html',
        controller: 'mainCtrl as mainCtrl'
    })
        .when('/about', {
            templateUrl: 'components/About/about.html',
            controller: 'aboutController as abtCtrl'
        })
        .when('/poi', {
            templateUrl: 'components/POI/poi.html',
            controller: 'poiCtrl as poiCtrl'
        })
        .when('/favorite', {
            templateUrl: 'components/Favorite/favorite.html',
            controller: 'FavoriteCtrl as FavoriteCtrl'
        })
        .when('/users', {
            templateUrl: 'components/Users/users.html',
            controller: 'usersCtrl as userCtrl'
        })
        .when('/fullpoi/:pointId', {
            templateUrl: 'components/POI/fullpoi.html',
            controller: 'fullpoiCtrl as fullpoiCtrl'
        })
        .otherwise({ redirectTo: '/' });
}])
    .config(function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('citiesApp');
    });

angular.module('citiesApp')
    .service('setHeadersToken', ['$http', function ($http) {

        this.set = function (token) {
            $http.defaults.headers.common['x-access-token'] = token;
        }

    }])
