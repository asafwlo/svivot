angular.module('citiesApp')
    .controller('fullpoiCtrl', ['$scope', 'localStorageService', '$http', '$route', '$routeParams', function ($scope, localStorageService, $http, $route, $routeParams) {
        self = this;

        $scope.pointId = $routeParams.pointId;


        $scope.logedIn = function (value) {

            token = localStorageService.get('token');
            if (!token) {
                return false;
            }
            return true;

        };

        $http.get('http://localhost:3000/POI/fullPointData/' + $scope.pointId)
            .then(function (response) {
                self.content = response.data[0];
                var mymap = L.map('mapid').setView(JSON.parse(response.data[0].location), 13);
                L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXNhZndsbyIsImEiOiJjamllZTRmMTMwbDltM3ZxbGhlM29kZDBpIn0.-0ZZJdMhqW_KMr0jixoh7A', {
                    attribution: '',
                    maxZoom: 18,
                    id: 'mapbox.streets'
                }).addTo(mymap);
                L.marker(JSON.parse(response.data[0].location)).addTo(mymap)
                    .openPopup();
                $scope.isReviewedFirst = function () {
                    if (self.content.firstReview == null)
                        return false;
                    else
                        return true;
                };
                $scope.isReviewedSecond = function () {
                    if (self.content.secondReview == null)
                        return false;
                    else
                        return true;
                };
                console.log(self.content);

                $http.get('http://localhost:3000/POI/allCategories/')
                    .then(function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            if (response.data[i].id === self.content.categoryID)
                                self.category = response.data[i].name;
                        }
                        console.log(self.category);
                    }, function (response) {
                        self.error = "someting wentWrong.";
                        console.log(self.error);
                    })

            }, function (response) {
                self.error = "someting wentWrong.";
                console.log(self.error);
            });


        $http.get('http://localhost:3000/RegisterUsers/favoritesPointsOfInterest')
            .then(function (response) {
                self.FavoritePOI = response.data;

                console.log(self.FavoritePOI);
            }, function (response) {
                self.FavoritePOI = "someting wentWrong.";
                console.log(self.FavoritePOI);
            });


        $scope.reviewDone = false;
        $scope.setReviewPOI = function (id, value) {
            self.reviewId = id;
            self.reviewPOI = value;
        };
        self.reviewRank = 0;
        self.setRank = function (value) {
            self.reviewRank = parseInt(value);
        };

        self.litHeart = function (value) {
            if (parseInt(value) <= self.reviewRank)
              return true;
            return false;
          };

        self.submitReview = function () {
            $http.put('http://localhost:3000/Review/addRateToPointOfInterest', JSON.stringify({ pointId: self.reviewId, rate: self.reviewRank }))
                .then(function (response) {
                    if (self.writtenReview != null && self.writtenReview != "") {
                        $http.put('http://localhost:3000/Review/addLiteralReviewToPointOfInterest', JSON.stringify({ pointId: self.reviewId, review: self.writtenReview }))
                            .then(function (response) {
                                if (response.data.success == false) {
                                    self.reviewStatus = "Something went wrong. please try again."
                                    $scope.reviewDone = true;
                                }
                                else {
                                    self.reviewStatus = "Thank you for your review!"
                                    $scope.reviewDone = true;
                                }
                            }, function (response) {
                                self.error = "someting wentWrong.";
                                console.log(self.error);
                                return false;
                            })
                    }
                    else {
                        if (response.data.success == false) {
                            self.reviewStatus = "Something went wrong. please try again."
                            $scope.reviewDone = true;
                        }
                        else {
                            self.reviewStatus = "Thank you for your review!"
                            $scope.reviewDone = true;
                        }
                    }
                }, function (response) {
                    self.error = "someting wentWrong.";
                    console.log(self.error);
                    return false;
                })
        };



    }]);