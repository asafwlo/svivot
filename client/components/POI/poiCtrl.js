angular.module('citiesApp')
    .controller('poiCtrl', ['$scope', 'localStorageService', '$http', '$route', function ($scope, localStorageService, $http, $route) {

        self = this;

        $scope.logedIn = function (value) {

            token = localStorageService.get('token');
            if (!token) {
                return false;
            }
            return true;

        };



        $http.get('http://localhost:3000/POI/allPointsOfInterest')
            .then(function (response) {
                self.content = response.data;
                console.log(self.content);
            }, function (response) {
                self.content = "someting wentWrong.";
                console.log(self.content);
            })

        $http.get('http://localhost:3000/RegisterUsers/favoritesPointsOfInterest')
            .then(function (response) {
                self.FavoritePOI = response.data;
                
                delFav = localStorageService.get('delfavorites');
                fav = localStorageService.get('favorites');
                self.numOfFav = self.FavoritePOI.length + fav.length - delFav.length;
                
                $scope.favorited = function (value) {
                    delFav = localStorageService.get('delfavorites');
                    if (delFav.indexOf(value) !== -1)
                        return false;
                    for (var i = 0; i < self.FavoritePOI.length; i++) {
                        if (value === self.FavoritePOI[i].id)
                            return true;
                    }
                    fav = localStorageService.get('favorites');
                    if (fav.indexOf(value) !== -1)
                        return true;
                    return false;
                };
                console.log(self.FavoritePOI);
            }, function (response) {
                self.FavoritePOI = "someting wentWrong.";
                console.log(self.FavoritePOI);
            });

        $scope.addFav = function (value) {
            delFav = localStorageService.get('delfavorites');
            if (delFav.indexOf(value) !== -1) {
                delFav.remove(value);
                localStorageService.set('delfavorites', delFav);
            }
            else {
                fav = localStorageService.get('favorites');
                fav.push(value);
                localStorageService.set('favorites', fav);
            }
            self.numOfFav = self.numOfFav + 1;
        };

        $scope.removeFav = function (value) {
            fav = localStorageService.get('favorites');
            var length = fav.length;
            fav.remove(value);
            if (fav.length === length) {
                delFav = localStorageService.get('delfavorites');
                delFav.push(value);
                localStorageService.set('delfavorites', delFav);
            }
            else
                localStorageService.set('favorites', fav);
            self.numOfFav = self.numOfFav - 1;
        };

        Array.prototype.remove = function () {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

        // $scope.addFav = function (value) {
        //     $http.put('http://localhost:3000/RegisterUsers/addFavoritePointOfInterestToEndOfList', JSON.stringify({ pointId: value }))
        //         .then(function (response) {
        //             self.refresh = "true";
        //             $route.reload();
        //             console.log(self.refresh);
        //         }, function (response) {
        //             self.content = "someting wentWrong.";
        //             console.log(self.content);
        //         })
        // };

        // $scope.removeFav = function (value) {
        //     $http.post('http://localhost:3000/RegisterUsers/removeFavoritePointOfInterest', JSON.stringify({ pointId: value }))
        //         .then(function (response) {
        //             self.refresh = "true";
        //             $route.reload();
        //             console.log(self.refresh);
        //         }, function (response) {
        //             self.content = "someting wentWrong.";
        //             console.log(self.content);
        //         })
        // };

        $http.get('http://localhost:3000/POI/allCategories')
            .then(function (response) {
                self.CategoriesDataLocal = response.data;
                self.CategoriesData = [];
                for (let index = 0; index < self.CategoriesDataLocal.length; index++) {
                    self.CategoriesData.push(self.CategoriesDataLocal[index].name);
                }
            }, function (response) {
                self.content = "someting wentWrong.";
                console.log(self.content);
            });

            $scope.reviewDone = false;
        $scope.setReviewPOI = function (id, value) {
            self.reviewId = id;
            self.reviewPOI = value;
        };
        self.reviewRank = 0;
        self.setRank = function (value) {
            self.reviewRank = parseInt(value);
        };

        self.litHeart = function (value) {
            if (parseInt(value) <= self.reviewRank)
                return true;
            return false;
        };

        self.submitReview = function () {
            $http.put('http://localhost:3000/Review/addRateToPointOfInterest', JSON.stringify({ pointId: self.reviewId, rate: self.reviewRank }))
                .then(function (response) {
                    if (self.writtenReview != null && self.writtenReview != "") {
                        $http.put('http://localhost:3000/Review/addLiteralReviewToPointOfInterest', JSON.stringify({ pointId: self.reviewId, review: self.writtenReview }))
                            .then(function (response) {
                                if (response.data.success == false) {
                                    self.reviewStatus = "Something went wrong. please try again."
                                    $scope.reviewDone = true;
                                }
                                else {
                                    self.reviewStatus = "Thank you for your review!"
                                    $scope.reviewDone = true;
                                }
                            }, function (response) {
                                self.error = "someting wentWrong.";
                                console.log(self.error);
                                return false;
                            })
                    }
                    else {
                        if (response.data.success == false) {
                            self.reviewStatus = "Something went wrong. please try again."
                            $scope.reviewDone = true;
                        }
                        else {
                            self.reviewStatus = "Thank you for your review!"
                            $scope.reviewDone = true;
                        }
                    }
                }, function (response) {
                    self.error = "someting wentWrong.";
                    console.log(self.error);
                    return false;
                })
        };

    }]);
