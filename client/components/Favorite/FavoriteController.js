angular.module('citiesApp')
  .controller('FavoriteCtrl', ['$scope', 'localStorageService', '$http', '$route', function ($scope, localStorageService, $http, $route) {

    self = this;

    $scope.count = 0;
    $scope.myFunc = function () {
      $scope.count++;
    };

    $scope.logedIn = function (value) {

      token = localStorageService.get('token');

      if (!token) {
        return false;
      }
      return true;

    };

    $http.get('http://localhost:3000/RegisterUsers/favoritesPointsOfInterest')
      .then(function (response) {
        fav = localStorageService.get('favorites');
        delFav = localStorageService.get('delfavorites');
        self.content = response.data;
        self.POI = [];
        self.category = [];
        for (var i = 0; i < fav.length; i++) {
          self.getPOI(fav[i], i);
        }
        self.content.remove(delFav);

        console.log(self.content);
      }, function (response) {
        self.error = "someting wentWrong.";
        console.log(self.error);
      })

    $scope.getOrder = function () {
      var table = document.getElementById("favTable");
      var array = []
      for (var i = 1; i < table.rows.length; i++) {
        array.push({ "pointId": parseInt(table.rows[i].cells[0].textContent), "place": i });

        // if (table.rows[i].cells.length == 9) {
        //   $http.put('http://localhost:3000/RegisterUsers/addFavoritePointOfInterestToEndOfList', JSON.stringify({ pointId: parseInt(table.rows[i].cells[0].textContent) }))
        //     .then(function (response) {
        //       self.refresh = "true";
        //       console.log(self.refresh);
        //     }, function (response) {
        //       self.content = "someting wentWrong.";
        //       console.log(self.content);
        //     })
        // }
      }
     // delFav = localStorageService.get('delfavorites');
      //for (var i = 0; i < delFav.length; i++) {
     //   self.removeFav(delFav[i]);
    //  }
      $http.put('http://localhost:3000/RegisterUsers/updateFavoritePointsOfInterestInSpecificOrder', JSON.stringify({ "array": array }))
        .then(function (response) {
          self.success = "New Order Saved Successfully.";
          var fav = [];
          var delfav = [];
          localStorageService.set('favorites', fav);
          localStorageService.set('delfavorites', delfav);
          console.log(response);
        }, function (response) {
          self.success = "Something went wrong. please refresh and try again.";
          self.error = "someting wentWrong.";
          console.log(self.error);
        })
    }

    //self.removeFav = function (value) {
    //  $http.post('http://localhost:3000/RegisterUsers/removeFavoritePointOfInterest', JSON.stringify({ pointId: value }))
     //   .then(function (response) {
     //     console.log(self.response);
     //   }, function (response) {
     //     self.error = "someting wentWrong.";
     //     console.log(self.error);
     //   })
   // };

    self.getPOI = function (value, index) {
      $http.get('http://localhost:3000/POI/fullPointData/' + value)
        .then(function (response) {
          self.POI[index] = response.data[0];
          self.setCat(index);


        }, function (response) {
          self.error = "someting wentWrong.";
          console.log(self.error);
        });
    }

    $scope.reviewDone = false;
    $scope.setReviewPOI = function (id, value) {
      self.reviewId = id;
      self.reviewPOI = value;
    };
    self.reviewRank = 0;
    self.setRank = function (value) {
      self.reviewRank = parseInt(value);
    };

    self.submitReview = function () {
      $http.put('http://localhost:3000/Review/addRateToPointOfInterest', JSON.stringify({ pointId: self.reviewId, rate: self.reviewRank }))
        .then(function (response) {
          if (self.writtenReview != null && self.writtenReview != "") {
            $http.put('http://localhost:3000/Review/addLiteralReviewToPointOfInterest', JSON.stringify({ pointId: self.reviewId, review: self.writtenReview }))
              .then(function (response) {
                if (response.data.success == false) {
                  self.reviewStatus = "Something went wrong. please try again."
                  $scope.reviewDone = true;
                }
                else {
                  self.reviewStatus = "Thank you for your review!"
                  $scope.reviewDone = true;
                }
              }, function (response) {
                self.error = "someting wentWrong.";
                console.log(self.error);
                return false;
              })
          }
          else {
            if (response.data.success == false) {
              self.reviewStatus = "Something went wrong. please try again."
              $scope.reviewDone = true;
            }
            else {
              self.reviewStatus = "Thank you for your review!"
              $scope.reviewDone = true;
            }
          }
        }, function (response) {
          self.error = "someting wentWrong.";
          console.log(self.error);
          return false;
        })
    };

    self.setCat = function (index) {
      $http.get('http://localhost:3000/POI/allCategories/')
        .then(function (response) {
          for (var i = 0; i < response.data.length; i++) {
            if (response.data[i].id === self.POI[index].categoryID) {
              self.category[index] = response.data[i].name;
              self.content.push({ id: self.POI[index].id, picture: self.POI[index].picture, name: self.POI[index].name, categoryID: self.POI[index].categoryID, rank: self.POI[index].rank, shortDesc: self.POI[index].shortDesc, cName: self.category[index], place: self.content.length, new: true });
              break;
            }
          }
          console.log(self.category[index]);
        }, function (response) {
          self.error = "someting wentWrong.";
          console.log(self.error);
        })
    };

    $scope.removeLocalFav = function (value) {
      fav = localStorageService.get('favorites');
      var length = fav.length;
      fav.remove(value);
      if (fav.length === length) {
        delFav = localStorageService.get('delfavorites');
        delFav.push(value);
        localStorageService.set('delfavorites', delFav);
      }
      else
        localStorageService.set('favorites', fav);
      self.numOfFav = self.numOfFav - 1;
      $route.reload();
    };

    Array.prototype.remove = function () {
      var what, a = arguments, L = a.length, ax;
      while (L && this.length) {
        what = a[--L];
        for (var j = 0; j < what.length; j++) {
          for (var i = 0; i < this.length; i++) {
            if (this[i].id === what[j])
              this.splice(i, 1);
          }
        }
      }
      return this;
    };

    self.litHeart = function (value) {
      if (parseInt(value) <= self.reviewRank)
        return true;
      return false;
    };
  }]);

