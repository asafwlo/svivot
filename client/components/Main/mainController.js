angular.module('citiesApp')
    .controller('mainCtrl', ['$scope', 'localStorageService', '$http', 'setHeadersToken', '$route', function ($scope, localStorageService, $http, setHeadersToken, $route) {
        self = this;
        token = localStorageService.get('token');
        fav = localStorageService.get('favorites');
        delFav = localStorageService.get('delfavorites');
        if (token) {
            setHeadersToken.set(token);
        }
        if (!fav) {
            var fav = [];
            localStorageService.set('favorites', fav);
        }
        if (!delFav) {
            var delFav = [];
            localStorageService.set('delfavorites', delFav);
        }

        //for the main page it self
        $scope.logedIn = function (value) {

            token = localStorageService.get('token');
            if (!token) {
                return false;
            }
            $scope.loggedUser = localStorageService.get('loggedUser');
            return true;

        };

        $scope.logOut = function (value) {
            localStorageService.remove('token');
            localStorageService.remove('loggedUser');
            localStorageService.remove('favorites');
            localStorageService.remove('delfavorites');
            $scope.loggedUser = 'Hello guest';
            window.location.href = "#main";
            $route.reload();
        };

        self.submit = function () {
            $http.post('http://localhost:3000/Users/login', JSON.stringify(self.user))
                .then(function (response) {
                    self.content = response.data;
                    if (self.content.success == false) {
                        $scope.error = "Incorrect username/password";
                    }
                    else {
                        setHeadersToken.set(self.content.token);
                        localStorageService.set('token', self.content.token);
                        localStorageService.set('loggedUser', 'Hello, ' + self.user.userName);
                        $scope.updateContent();
                    }


                }, function (response) {
                    self.content = "someting wentWrong.";
                    console.log(self.content);
                    return false;
                })

            console.log('User clicked submit with ', self.user);
        };

        $scope.updateContent = function () {
            $http.get('http://localhost:3000/POI/exploringPointsOfinterest')
                .then(function (response) {
                    self.content = response.data;
                    console.log(self.content);
                }, function (response) {
                    self.content = "someting wentWrong.";
                    console.log(self.content);
                })

            token = localStorageService.get('token');
            if (token) {
                $http.get('http://localhost:3000/RegisterUsers/popularPointsOfInterestByUserCategories')
                    .then(function (response) {
                        if (response.data.success == false) {
                            $scope.logOut();
                        }
                        else {
                            self.categoriesPoints = response.data;
                            console.log(self.categoriesPoints);
                        }
                    }, function (response) {
                        self.content = "someting wentWrong.";
                        console.log(self.content);
                    })


                $http.get('http://localhost:3000/RegisterUsers/lastPointsOfInterestSaved')
                    .then(function (response) {
                        if (response.data.success == false) {
                            $scope.logOut();
                        }
                        else {
                            self.savedPoints = response.data;
                            console.log(self.savedPoints);
                        }
                    }, function (response) {
                        self.content = "someting wentWrong.";
                        console.log(self.content);
                    })
            }
        }


        $scope.updateContent();

        //for retriving the password

        $scope.resetParams = function () {
            $scope.questionsFound = false;
            $scope.final = false;
        }

        $scope.resetParams();

        self.submitpass = function () {
            $http.post('http://localhost:3000/Users/QustionsByUserName', JSON.stringify({ userName: self.passUserName }))
                .then(function (response) {
                    self.questions = response.data;
                    if (self.questions.success == false) {
                        $scope.passError = "Incorrect username";
                    }
                    else if (self.questions.length == 0) {
                        $scope.passError = "the username in not recognized in the system.";
                    }
                    else {
                        if (self.questions.length != 2) {
                            $scope.passError = "the username in not valid.";
                        }
                        else {
                            $scope.passError = "";
                            $scope.questionsFound = true;

                            $scope.q1 = self.questions[0].text;
                            $scope.q2 = self.questions[1].text;
                        }
                    }

                }, function (response) {
                    self.content = "someting wentWrong.";
                    console.log(self.content);
                    return false;
                })

            console.log('User clicked submit with ', self.user);
        };

        self.submitretriveQ = function () {
            $http.post('http://localhost:3000/Users/restorePassword', JSON.stringify({ userName: self.passUserName, q1: self.questions[0].questionId, q2: self.questions[1].questionId, ans1: self.q1ans, ans2: self.q2ans }))
                .then(function (response) {
                    if (response.data.success == false) {
                        $scope.retriveQErr = "the answers were not correct. sorry. "
                    }
                    else {
                        $scope.final = true;
                        self.retrivePassword = response.data.password;
                    }
                }, function (response) {
                    self.content = "someting wentWrong.";
                    console.log(self.content);
                    return false;
                });
        };
        $scope.reviewDone = false;
        $scope.setReviewPOI = function (id, value) {
            self.reviewId = id;
            self.reviewPOI = value;
        };
        self.reviewRank = 0;
        self.setRank = function (value) {
            self.reviewRank = parseInt(value);
        };

        self.litHeart = function (value) {
            if (parseInt(value) <= self.reviewRank)
                return true;
            return false;
        };

        self.submitReview = function () {
            $http.put('http://localhost:3000/Review/addRateToPointOfInterest', JSON.stringify({ pointId: self.reviewId, rate: self.reviewRank }))
                .then(function (response) {
                    if (self.writtenReview != null && self.writtenReview != "") {
                        $http.put('http://localhost:3000/Review/addLiteralReviewToPointOfInterest', JSON.stringify({ pointId: self.reviewId, review: self.writtenReview }))
                            .then(function (response) {
                                if (response.data.success == false) {
                                    self.reviewStatus = "Something went wrong. please try again."
                                    $scope.reviewDone = true;
                                }
                                else {
                                    self.reviewStatus = "Thank you for your review!"
                                    $scope.reviewDone = true;
                                }
                            }, function (response) {
                                self.error = "someting wentWrong.";
                                console.log(self.error);
                                return false;
                            })
                    }
                    else {
                        if (response.data.success == false) {
                            self.reviewStatus = "Something went wrong. please try again."
                            $scope.reviewDone = true;
                        }
                        else {
                            self.reviewStatus = "Thank you for your review!"
                            $scope.reviewDone = true;
                        }
                    }
                }, function (response) {
                    self.error = "someting wentWrong.";
                    console.log(self.error);
                    return false;
                })
        };


    }]);
