angular.module('citiesApp')
  .controller('usersCtrl', ['$http', '$location', 'setHeadersToken', 'localStorageService','$window', function ($http, $location, setHeadersToken, localStorageService,$window) {

    self = this;

    $http.get('http://localhost:3000/Users/CountriesOptionalls')
      .then(function (response) {
        self.CountriesLocal = response.data.Countries.Country;
        self.CountriesData = [];
        for (let index = 0; index < self.CountriesLocal.length; index++) {
          self.CountriesData.push(self.CountriesLocal[index].Name);

        }

        console.log(self.CountriesData);
      }, function (response) {
        self.content = "someting wentWrong.";
        console.log(self.content);
      })


    $http.get('http://localhost:3000/Users/allQuestionOptionalls')
      .then(function (response) {
        self.QuestionsDataLocal = response.data;
        self.QuestionsData = [];
        for (let index = 0; index < self.QuestionsDataLocal.length; index++) {
          self.QuestionsData.push(self.QuestionsDataLocal[index].text);
        }
      }, function (response) {
        self.content = "someting wentWrong.";
        console.log(self.content);
      })

    $http.get('http://localhost:3000/POI/allCategories')
      .then(function (response) {
        self.CategoriesDataLocal = response.data;
        self.CategoriesData = [];
        for (let index = 0; index < self.CategoriesDataLocal.length; index++) {
          self.CategoriesData.push(self.CategoriesDataLocal[index].name);
        }
      }, function (response) {
        self.content = "someting wentWrong.";
        console.log(self.content);
      })

    this.selection = [];

    self.registerUserForm = function () {
      message = "";
      counter = 0;
      var okay = true;

      if (!self.user.collactionCat) {
        message = message + "Please check 2 categories at least.  ";
        okay = false;
      }
      else {
        categoriesPreFormat = []
        for (var key in self.user.collactionCat) {
          if (self.user.collactionCat.hasOwnProperty(key)) {
            if (self.user.collactionCat[key] == true) {
              categoriesPreFormat[counter] = key;
              counter++;
            }
          }
        }
        if (counter < 2) {
          message = message + "Please check 2 categories at least.  ";
          okay = false;
        }
      }
      if (!self.user.firsqQ) {
        message = message + "Please choose the first question.  ";
        okay = false;
      }
      if (!self.user.secondQ) {
        message = message + "Please choose the second question.  ";
        okay = false;
      }
      if (self.user.secondQ && self.user.firsqQ && self.user.firsqQ.id == self.user.secondQ.id) {
        message = message + "Please choose two different questions.  ";
        okay = false;
      }
      if (!self.user.country || self.user.country.length != 1) {
        message = message + "Please choose a valid country.  ";
        okay = false;
      }


      if (okay) {
        self.messErr = "";
        categoriesFormated = JSON.stringify(categoriesPreFormat);
        jsonMess = JSON.stringify({
          firstName: self.user.firstName, lastName: self.user.lastName,
          City: self.user.city, Country: self.user.country[0],
          Email: self.user.email, categories: categoriesFormated,
          q1: self.user.firsqQ.id, q2: self.user.secondQ.id,
          ans1: self.user.firstAns, ans2: self.user.secondAns
        })

        $http.post('http://localhost:3000/Users/registerNewUser', jsonMess)
          .then(function (response) {
            self.userName = response.data[0].userName;
            self.password = response.data[0].password;
          }, function (response) {
            self.content = "someting wentWrong.";
            console.log(self.content);
          })

        
        self.logedIn = true;

      }
      else {
        self.messErr = message;
      }
    };

    self.submit = function () {
      $http.post('http://localhost:3000/Users/login', JSON.stringify(self.user))
          .then(function (response) {
              self.content = response.data;
              if (self.content.success == false) {
                  self.LoginFormerror = "Incorrect username/password";
              }
              else {
                  setHeadersToken.set(self.content.token);
                  localStorageService.set('token', self.content.token);
                  localStorageService.set('loggedUser', 'Hello, ' + self.user.userName);
                  $window.location.href = '#/';
              }


          }, function (response) {
              self.content = "someting wentWrong.";
              console.log(self.content);
              return false;
          })

  };


  }]);